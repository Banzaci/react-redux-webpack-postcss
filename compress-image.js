const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');

imagemin(['./images/*.{jpg,png}'], './dist', {
    plugins : [
        imageminJpegtran(),
        imageminPngquant()
    ]
}).then(files => {
    console.log('Image(s) compressed = success.');
}).catch((err) => {
    console.log(err);
});
