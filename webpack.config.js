const webpack             = require('webpack');
const LiveReloadPlugin    = require('webpack-livereload-plugin');
const precss              = require('precss');
const autoprefixer        = require('autoprefixer');
const cssImport           = require('postcss-import');
const cssSelector         = require('postcss-custom-selectors');
const cssnano             = require('cssnano');
const ExtractTextPlugin   = require('extract-text-webpack-plugin');
const htmlBuilder         = require('./index-html-creator-postcss');
const cssAssets           = require('postcss-assets');
//http://csswizardry.com/2016/06/improving-your-css-with-parker/?utm_source=CSS-Weekly&utm_campaign=Issue-218&utm_medium=email
const config = {
    context: __dirname,
    entry: [
        './resources/javascript/main.js',
        './resources/stylesheet/main.css',
    ],
    devtool: "inline-sourcemap",
    output: {
        path: __dirname + "/dist/",
        filename: "javascript/main.min.js"
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query:{
                    presets:['es2015', 'react']
                }
            },
            {
                test: /\.css$/, loader: ExtractTextPlugin.extract('style-loader', 'css-loader!postcss-loader')
            },
            {
                test: /.*\.(gif|png|jpe?g|svg)$/i,
                loaders: [
                    'file?optimizationLevel=5&hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack'
                ]
            }
        ]
    },
    postcss: function (webpack) {
        return [
            cssImport({ addDependencyTo : webpack }),
            //require('postcss-at2x')(),
            // require('postcss-sprites').default({
            //     stylesheetPath  : './dist/stylesheet',
            //     spritePath      : './dist/images/'
            // }),//{ retina: true }
            precss,
            autoprefixer,
            cssnano,
            cssSelector,
            cssAssets
        ];
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    plugins:[
        new LiveReloadPlugin({}),
        new ExtractTextPlugin('stylesheet/style.min.css'),
        // new htmlBuilder({
        //     css:'stylesheet/style.min.css',
        //     content:'<div id="app" class="container"></div>',
        //     js:'javascript/main.min.js',
        //     dest:'./dist',
        //     title:'Testar'
        // })
    ]
};

if(process.env.NODE_ENV === 'production'){
    config.devtool = 'eval-source-map';
    config.plugins.push(new webpack.optimize.UglifyJsPlugin());//{ mangle: false, sourcemap: false }?
}

module.exports = config;

/*
{
    test: /\.(jpe?g|png|gif|svg)$/i,
    loaders: [
        'file?hash=sha512&digest=hex&name=[hash].[ext]',
        'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
    ]
}
*/
