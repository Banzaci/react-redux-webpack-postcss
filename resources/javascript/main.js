import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import App from './containers/app';
import configureStore from './store';
//import '../stylesheet/main.css';

const store = configureStore();

ReactDom.render(
    <Provider store={ store }>
        <App />
    </Provider>,
    document.getElementById('app')
)
