import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';//Async Action Creator, http://stackoverflow.com/questions/35411423/how-to-dispatch-a-redux-action-with-a-timeout/35415559#35415559
import createLogger from 'redux-logger';
import reducers from './reducers/reducers';

export default function configureStore( initialState = {} ) {
    const store = createStore(
        reducers,
        initialState,
       applyMiddleware( thunkMiddleware, createLogger())
    )
    return store;
}
