import { combineReducers } from 'redux';
import carousel from '../modules/carousel';
import statistics from '../modules/statistics';

export default combineReducers({
    [carousel.constants.NAME]: carousel.reducer,
    [statistics.constants.NAME]: statistics.reducer
});
