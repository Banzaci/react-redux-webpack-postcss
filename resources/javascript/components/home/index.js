import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import CarouselModule from '../../modules/carousel';
import StatisticsModule from '../../modules/statistics';
import './home.css';

class Home extends Component {

  constructor(props) {
    super(props);
    this.props = props;
  }

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(this.incrementActions());
  }

  incrementActions() {
    return dispatch => {
      dispatch( CarouselModule.actions.fetchIfNeeded( CarouselModule.constants.NAME) )
      dispatch( StatisticsModule.actions.fetchIfNeeded( StatisticsModule.constants.NAME) )
    }
  }

  render(){
    const { carousel: { images = [] }, statistics: { documents = [] } } = this.props;
    console.log(images);
    return (
      <section>
        <span className="home-title">Home page</span>
        { ( images ) ? <CarouselModule.component images={ images } /> : 'Loading....' }
        { ( documents ) ? <StatisticsModule.component documents={ documents } /> : 'Loading....' }
      </section>
    )
  }
}

Home.propTypes = {
  carousel    : PropTypes.object.isRequired,
  statistics  : PropTypes.object.isRequired,
  dispatch    : PropTypes.func.isRequired
}

function mapStateToProps(state) {
  const { carousel, statistics } = state;
  return {
    carousel,
    statistics
  }
}

export default connect(mapStateToProps)(Home);
