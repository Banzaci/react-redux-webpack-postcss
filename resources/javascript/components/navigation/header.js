import React from 'react';
import { Router, Route, Link } from 'react-router';
import './header.css';

const Header = (props) => {

    return (
        <nav>
            <Link className="link" to="/">Home</Link>
            <Link className="link" to="/listConsignments">Sändningar</Link>
        </nav>
    )
}
export default Header;
