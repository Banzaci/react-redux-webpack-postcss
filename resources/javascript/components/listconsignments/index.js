import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import ListConsignment from '../../modules/listconsignments';
import DatePicker from '../../modules/datepicker';
import SearchConsignments from '../../modules/consignment-input-search';
import moment from 'moment';

var contextTypes = {
    print: PropTypes.func.isRequired
}

var myContext = {
    print: (m) => (m)
}

@context(contextTypes, myContext)
export default class ListConsignments extends Component {

    constructor(props) {

        super(props);
        this.props = props;

        this.state = {
            dateType        : 'REGISTERED',
            startDate       : moment('2016-04-01T00:00:00+02:00'),
            endDate         : moment(),
            internalStates  : ['REGISTERED', 'REGISTERED_INCOMPLETE'],
            selectedTags    : [],
            REGISTERED      : '1'
        };

        this.fetch                  = ListConsignment.actions.fetchIfNeeded;
        this.SEARCH_CONSIGNMENTS    = ListConsignment.constants.SEARCH_CONSIGNMENTS;
        this.LIST_CONSIGNMENT_NAME  = ListConsignment.constants.NAME;
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch( this.fetch( this.LIST_CONSIGNMENT_NAME, this.state ) )
    }

    componentWillReceiveProps( nextProps ) {}

    onSelectedConsignment( consignments ){
        const { dispatch } = this.props;
        dispatch( this.fetch( this.SEARCH_CONSIGNMENTS, Object.assign({}, this.state, { selectedTags:consignments}) ) )
    }

    onStartDate( date ){

        const { dispatch } = this.props;

        this.setState({
            startDate : date
        });

        dispatch( this.fetch( this.SEARCH_CONSIGNMENTS, Object.assign({}, this.state, { startDate:date }) ) )
    }

    onEndDate( date ){

        const { dispatch } = this.props;

        this.setState({
            endDate : date
        });

        dispatch( this.fetch( this.SEARCH_CONSIGNMENTS, Object.assign({}, this.state, { endDate:date }) ) );
    }

    render(){
        const { listConsignments } = this.props;
        const { consignments = [] } = listConsignments;

        return (
            <section>
                <SearchConsignments.component onSelectedConsignment={ (e) => this.onSelectedConsignment(e) } />
                <DatePicker.component selectedDate={ this.state.startDate } dateHandler={ (e) => this.onStartDate(e) } />
                <DatePicker.component selectedDate={ this.state.endDate } dateHandler={ (e) => this.onEndDate(e) } />
                { ( consignments ) ? <ListConsignment.component consignments={ consignments } /> : 'Loading....' }
            </section>
        )
    }
}

ListConsignments.propTypes = {
    listConsignments : PropTypes.object.isRequired,
    dispatch         : PropTypes.func.isRequired
}

function mapStateToProps(state) {
    const { listConsignments } = state;
    return {
        listConsignments
    }
}

export default connect(mapStateToProps)(ListConsignments);
/*

 */
