import Template from './template';
import Home from '../components/home/index';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import React from 'react';

export default (props) => {
    return (
        <Router history={ hashHistory }>
            <Route path="/" component={ Template }>
                <IndexRoute component={ Home } />
            </Route>
        </Router>
    );
};
/* <Route path="listConsignments" component={ ListConsignments } /> */
