import React from 'react';
import Header from '../components/navigation/header';

export default (props) => {
    return (
        <div className="wrapper">
            <Header />
            { props.children }
        </div>
    );
};
