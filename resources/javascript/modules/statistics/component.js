import React from 'react';

const Component = (props) => {
    const { documents } = props;

    const markup = documents.map((document, index) => {
        return (
            <article key={ index }>
                { document }
            </article>
        );
    });

    return (
        <section>
            { markup }
        </section>
    )
}

export default Component;
