import * as actions from './actions';
import * as constants from './constants';
import component from './component';
import reducer from './reducer';

export default { actions, constants, reducer, component };