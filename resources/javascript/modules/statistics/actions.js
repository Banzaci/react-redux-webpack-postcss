import * as constants from './constants';

function request(state) {
    return {
        type : constants.LOADING,
        state
    }
}

function fetching( state ) {

    return dispatch => {

        dispatch( request( state ) );

        setTimeout(() => {
            dispatch({
                type        : constants.LOAD_SUCCESS,
                state,
                documents   : ['doc1', 'doc2', 'doc3'],
                receivedAt  : Date.now()
            });
        }, 1000);
    }
}

function shouldFetch( state, action ) {
    const stateOwner = state[action];
    if (Object.keys(stateOwner).length < 1) {
        return true;
    }
    if (stateOwner.isFetching) {
        return false
    }
    return stateOwner.didInvalidate;
}

export function fetchIfNeeded( state ) {
    return (dispatch, getState) => {
        if (shouldFetch( getState(), state) ) {
            return dispatch(fetching(state))
        }
    }
}


