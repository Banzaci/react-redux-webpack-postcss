import * as constants from './constants';

function request(state) {
    return {
        type : constants.LOADING,
        state
    }
}

function fetching( state ) {

    return dispatch => {

        dispatch( request( state ) );

        return new Promise((resolve, reject)=>{
            setTimeout(() => {
                resolve(dispatch({
                    type        : constants.LOAD_SUCCESS,
                    state,
                    images      : ['img1.jpg', 'img2.jpg', 'img3.jpg'],
                    receivedAt  : Date.now()
                }));
            }, 1000);
        });
    }
}

function shouldFetch( state, action ) {
    const stateOwner = state[action];
    if (Object.keys(stateOwner).length < 1) {
        return true;
    }
    if (stateOwner.isFetching) {
        return false
    }
    return stateOwner.didInvalidate;
}

export function fetchIfNeeded( state ) {
    return (dispatch, getState) => {
        if (shouldFetch( getState(), state) ) {
            return dispatch(fetching(state))
        }
    }
}
