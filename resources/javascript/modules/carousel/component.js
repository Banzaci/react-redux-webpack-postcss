import React from 'react';

const Component = (props) => {
    const { images } = props;

    const markup = images.map((image, index) => {
        return (
            <article key={ index }>
                { image }
            </article>
        );
    });

    return (
        <section>
            { markup }
        </section>
    )
}

export default Component;
