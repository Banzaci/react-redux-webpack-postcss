import * as constants from './constants';

function stateBuilder( state = { isFetching: false, didInvalidate: false, images:[] }, action = {} ){

    switch ( action.type ) {
        case constants.LOAD_FAIL:
            return Object.assign({}, state, {
                didInvalidate   : true
            });
            break;
        case constants.LOADING :
            return Object.assign({}, state, {
                isFetching      : true,
                didInvalidate   : false
            });
            break;
        case constants.LOAD_SUCCESS :
            return Object.assign({}, state, {
                images          : action.images,
                isFetching      : false,
                didInvalidate   : false
            });
            break;
        default :
            return state;
    }
}

export default function reducer( state = {}, action = {} ) {
    switch ( action.type ) {
        case constants.LOAD_FAIL    :
        case constants.LOADING      :
        case constants.LOAD_SUCCESS :
            return Object.assign({}, state, stateBuilder( state[ action.state ], action ));
            break;
        default:
            return state;
    }
}
