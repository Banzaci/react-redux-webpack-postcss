export const NAME           = 'carousel';
export const LOADING        = `${NAME}/LOADING`;
export const LOAD_SUCCESS   = `${NAME}/LOAD_SUCCESS`;
export const LOAD_FAIL      = `${NAME}/LOAD_FAIL`;
