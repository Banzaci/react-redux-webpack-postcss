export default class Request {

    constructor(url){
        this.url = url;
        this.defaultRequest = {
            credentials : 'include',
            method      : 'GET',
            headers     : {
                'Content-Type' : 'application/json; charset=utf-8'
            }
        }
    }

    post( body ){

        const requestBody   = Object.assign({}, this.defaultRequest, {
            method  : 'POST',
            body    : JSON.stringify(body)
        });

        const requestUrl    = `/np/api/${this.url}`;

        return fetch(requestUrl, requestBody )
            .then( response => response.json() );
    }

    get( body ){

        const params        = factorQueryParams(body);
        const requestBody   = Object.assign( {}, this.defaultRequest );
        const requestUrl    = `/np/api/${this.url}${params}`;

        return fetch( requestUrl, requestBody )
            .then( response => response.json() )
    }
}

function factorQueryParams( params={} ){
    const paramKeys  = Object.keys( params );
    const paramList  = paramKeys.map((key) => `${key}=${ params[key] }`);
    return (paramList.length > 0) ? `?${paramList.join('&')}` : '';
}