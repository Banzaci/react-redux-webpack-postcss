import { expect } from 'chai';
import TestUtil from 'react-addons-test-utils';

import carousel from '../../javascript/modules/carousel';
import configureStore from '../../javascript/store';

const store = configureStore();

describe('carousel', () => {
    it('should dispatch an async event', (done) => {
      const dispatch = store.dispatch( carousel.actions.fetchIfNeeded( carousel.constants.NAME ) );
      dispatch.then(response => {
          expect( response ).to.have.all.keys('images', 'type', 'state', 'receivedAt');
          done();
      });
    });
});

//http://chaijs.com/api/bdd/
//http://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html
