import React from 'react';
import sinon from 'sinon';
import { expect } from 'chai';
import { shallow, mount, render } from 'enzyme';
import configureStore from '../../javascript/store';
import Home from '../../javascript/components/home';
import carousel from '../../javascript/modules/carousel';

const store = configureStore();

describe('Home', () => {
  let dispatch;
  const wrapper = mount(<Home store={ store } />);
  before(() => {
    dispatch = sinon.stub();
    // stub store.dispatch
    // sinon.stub(carousel.actions, 'fetchIfNeeded', () => [1,2,3]);
  });

  it('should contain header text', (done) => {
    expect(wrapper.find('.home-title').text()).to.equal('Home page');
    done();
  });

  it('should call images api', (done) => {
      expect(dispatch).to.have.been.calledOnce;
      done();
  });
});
