var compression     = require('compression');
var express         = require('express');
var bodyParser      = require('body-parser');
var port            = process.env.PORT || 3000;
var app             = express();
var cacheTime       = 86400000*7;
var ecstatic        = require('ecstatic');

app.use(compression());

app.use(function (req, res, next) {
    if (req.url.match(/^\/(css|js|img|font)\/.+/)) {
        rres.header('Cache-Control', 'no-transform,public,max-age=300,s-maxage=900');
        res.header('Vary', 'Accept-Encoding');
    }
    next();
});


//app.use(ecstatic({ root: __dirname + '/dist', gzip:true, cache:3600 }));
app.use('/', express.static(__dirname + '/dist', { maxAge: cacheTime }));//etag: false, , lastModified:false

app.listen(port, function () {
    console.log('Example app listening on port ' + port);
});
